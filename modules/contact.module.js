
const contacts = [ 
    { id : 1, nom : "Théophile", prenom : "Defer", email : "theo.f@gmail.com", tel : "0472727272" } 
]

let currentContactId = 1

const getAll = () => {
    return contacts
}

const createContact = (contact) => {
    contacts.push({...contact, id : ++currentContactId })
    return contact
}

module.exports = { getAll, createContact }