console.log('------------ Demo Web Server ------------');

// * Etape 1 - Création Serveur : Importer le module htpp
const http = require('http')

// * Utils
//Pour découper la requête et obtenir les infos qui nous intéressent
const urlParser = require("url")
const queryString = require("querystring")

//Import mes modules
const contactModule = require("./modules/contact.module")

// * Etape 2 - Création Serveur : Créer le serveur
const app = http.createServer((req, res) => {
    //On récupère chacune des requêtes
    //console.log(req)
    console.log(req.url)
    //Soucis pour l'url, je récupère tout le tronçon, sauf que j'ai besoin de dissocier url et query
    const reqUrl = urlParser.parse(req.url).pathname
    const reqQuery = urlParser.parse(req.url).query
    const reqMethod = req.method
    console.log(reqUrl) //Le chemin, partie après localhost:8080 (/, /contact, /about etc etc)
    console.log(reqQuery) //Tout ce qu'il y a après le ? dans une request url (?limit=20&offset=0)
    console.log(reqMethod) //Valeurs possible : GET / POST
    
    //Une fois qu'on a tout découpé, on peut mettre en place notre logique de routing
    if( reqUrl === '/' && reqMethod === 'GET') {

        console.log('Bienvenue sur la page d\'accueil')

        const trainers = [
            { firstname : "Aude", lastname : "Beurive" },
            { firstname : "Pierre", lastname : "Santos"},
            { firstname : "Gavin", lastname : "Chaineux"},
            { firstname : "Steve", lastname : "Lorent" }
        ]
        let lesLi = ''
        trainers.forEach(trainer => { lesLi += `<li> ${trainer.firstname} ${trainer.lastname} </li>`})
        console.log(lesLi);


        const TODAY = new Date()
        //Création du contenu HTML à envoyer en réponse
        const content = `
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Accueil</title>
        </head>
        <body>
            <h1>Bienvenue sur la page d'accueil</h1>
            <h2>Aujourd'hui nous sommes le ${TODAY.toLocaleDateString('fr-BE', { weekday : 'long', day : 'numeric', month : 'long', year : 'numeric' })}</h2>
            <ul>
            ${ trainers.map(trainer => `<li> ${trainer.firstname} ${trainer.lastname}</li>`).join("") }
            <!-- ${ trainers.reduce( (acc, trainer ) => { acc += `<li> ${trainer.firstname} ${trainer.lastname}</li>`; return acc}, '')} -->
            </ul>
            </body>
            </html>
        `

        //Paramétrage de la response (res) pour lui indiquer que c'est du HTML qu'on envoie
        res.writeHead(200, { 
            "Content-type" : "text/html" //On précise qu'on envoie du text en format HTML
        })

        //Terminer la requête, on lui fournissant le contenu
        res.end(content)


    } else if ( reqUrl === '/contact') {
        if(reqMethod === 'GET') {

            console.log('Bienvenue sur la page contact');

            const content = `
            <!DOCTYPE html>
            <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Contact</title>
                </head>
                <body>
                    <h1>Me contacter : </h1>
                    <form method="POST">
                        <div>
                            <label for="name">Votre prénom : </label>
                            <input id="name" name="name" type="text">
                        </div>
                        <div>
                            <label for="msg">Votre message : </label>
                            <textarea id="msg" name="msg"></textarea>
                        </div>
                        <div>
                            <input type="submit" value="Envoyer le message">
                        </div>
                    </form>
                </body>
            </html>
            `
            res.writeHead(200, { 
                "Content-type" : "text/html" //On précise qu'on envoie du text en format HTML
            })
    
            res.end(content)

        }
        if(reqMethod === 'POST') {

            console.log('Récupération et envoi des données');
            let data = ''

            //On écoute un évènement qui s'appelle data (déclenché lors de réception des données du formulaire)
            req.on('data', (form) => {
                console.log('FORMULAIRE : ', form);
                console.log(form.toString('utf-8'));
                data += form.toString('utf-8')
                console.log('DATA : ', data);
            })

            //On écoute un évènement qui s'appelle end (déclenché à la fin du traitement de toutes données)
            req.on('end', () => {
                console.log('DATA END : ', data);
                const messageToAdd = queryString.parse(data)
                //TODO (idéalement mais on va le faire because c'est chiant en node pur) -> Ajouter en db
                console.log(messageToAdd);

                //Une fois qu'on a traité les données
                res.writeHead(301 , {
                    "Location" : "/" //Redirection vers la homepage
                })
                res.end()
            })

        }
    } else if ( reqUrl === '/list-contact' && reqMethod === 'GET' ) {
        //récupérer la liste
        const contacts = contactModule.getAll()

        //créer le contenu
        const content = `
            <!DOCTYPE html>
            <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Contact</title>
                </head>
                <body>
                <h1> Liste des contacts : </h1>
                <table>
                    <thead>
                        <tr>
                            <th> Nom </th>
                            <th> Prénom </th>
                            <th> Email </th>
                            <th> Téléphone </th>
                        </tr>
                     </thead>
                    <tbody>                    
                        ${ contacts.map(contact => `<tr> <td>${contact.nom}</td> <td>${contact.prenom}</td> <td>${contact.email}</td> <td>${contact.tel}</td> </tr>` ).join()  } 
                    </tbody>
                 </table>
                 <a href="/add-contact">Ajouter un contact</a>
                </body>
            </html>
        `
        //l'envoyer en réponse
        res.writeHead(200, {
            'Content-type' : 'text/html'
        })

        res.end(content)

    }  else if (reqUrl === "/add-contact") {
        if( reqMethod === 'GET') {
            const content = `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Document</title>
            </head>
            <body>
                <h1>Ajouter un contact : </h1>
                <form method="post">
                    <div>
                        <label for="nom">Nom : </label>
                        <input id="nom" name="nom" type="text">
                    </div>
                    <div>
                        <label for="prenom">Prénom :</label>
                        <input id="prenom" name="prenom" type="text">
                    </div>
                    <div>
                        <label for="email">Email : </label>
                        <input id="email" name="email" type="email">
                    </div>
                    <div>
                        <label for="tel">Téléphone : </label>
                        <input id="tel" name="tel" type="tel">
                    </div>
                    <div>
                        <input type="submit" value="Ajouter">
                    </div>
                </form>
            </body>
            </html>`

            // res.writeHead(200, {
            //     'Content-type' : 'text/html'
            // })
    
            res.end(content)

        }
        if( reqMethod === 'POST') {
            let data = ''

            req.on('data', (form) => {
                data += form.toString()
            })

            req.on('end', () => {
                const contactToAdd = queryString.parse(data)
                const trueContactToAdd = {
                    nom : contactToAdd.nom,
                    prenom : contactToAdd.prenom,
                    email : contactToAdd.email,
                    tel : contactToAdd.tel
                }
                contactModule.createContact(trueContactToAdd)

                res.writeHead(301, {
                    "Location" : "/list-contact"
                })
                res.end()
            })
        }
    }
    else {

        console.log('404 Not Found');

        const content = `
        <!DOCTYPE html>
            <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Contact</title>
                </head>
                <body>
                    <h1> 404 Not Found 🤖 </h1>
                </body>
                </html>
        `
        res.writeHead(200, { 
            "Content-type" : "text/html" //On précise qu'on envoie du text en format HTML
        })

        res.end(content)
    }

})

// * Etape 3 - Création Serveur : Lancer/Ecouter le server
app.listen(8080, () => {
     console.log('Server started on port : 8080')
} )